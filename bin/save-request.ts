#!/usr/bin/env node
import 'source-map-support/register';
import SaveRequestStack from '../lib/save-request-stack';
import { App } from  '@aws-cdk/core';

const app = new App();
new SaveRequestStack(app, 'SaveRequestStack');
app.synth();
